# pptshow

#### 介绍
Java生成PPT文档工具包，支持2010版PPTX新特性 

#### 先感受下
比如下面这个PPT就是完全使用本Jar包制作的  
包括这个视频也是通过Jar包自动生成的  
![演示视频](https://gitee.com/qiruipeng/pptshow/raw/master/example.mp4 "演示视频")

#### 功能支持
##### PPT基础编辑功能
1. PPT多个页面
2. PPT背景音乐
3. PPT切换动画、自动定时切换
4. PPT文字、图片、形状等元素的插入支持
5. PPT元素的入场动画

##### PPT拓展功能
1. PPT生成图片（字体不会乱，仅在Windows环境下可用）
2. PPT生成MP4视频（字体不会乱，仅在Windows环境下可用）

##### PPT读取
1. 根据指定的PPT页面文件读取信息

#### 10秒上手
您可以使用maven添加jar包引用，或者直接下载jar包并手动导入
##### Maven坐标【推荐】
```xml
<dependency>
  <groupId>cc.pptshow</groupId>
  <artifactId>pptshow</artifactId>
  <version>1.3</version>
</dependency>
```
##### 下载并手动导入【不推荐】
下载地址：https://s01.oss.sonatype.org/service/local/repositories/releases/content/cc/pptshow/pptshow/1.3/pptshow-1.3.jar  
  
导入成功后新建一个Main类，复制以下代码并粘贴其中：
 ```java
 import cc.pptshow.ppt.domain.*;
import cc.pptshow.ppt.element.impl.*;
import cc.pptshow.ppt.show.PPTShow;
import cc.pptshow.ppt.show.PPTShowSide;

public class Main {

    public static void main(String[] args) {
        //新建一个PPT对象
        PPTShow pptShow = PPTShow.build();
        //新建一页PPT
        PPTShowSide side = PPTShowSide.build();
        
        //创建一个行内文本对象，文字设定为Hello World
        PPTInnerText pptInnerText = PPTInnerText.build("Hello World");
        //创建一个行内文本样式对象，让文本颜色为红色
        PPTInnerTextCss pptInnerTextCss = PPTInnerTextCss.build().setColor("FF00000");
        //绑定行内文本和样式对象
        pptInnerText.setCss(pptInnerTextCss);
        
        //通过行内文本创建一个行文本对象，并通过行文本对象创建文本对象
        PPTText pptText = PPTText.build(PPTInnerLine.build(pptInnerText));
        //在PPT页面中添加文本对象
        side.add(pptText);
        //在PPT里面添加PPT页面
        pptShow.add(side);
        
        //输出到文件
        pptShow.toFile("C:/Users/qrp19/Desktop/test4.pptx");
    }

}

 ```

#### Jar包说明
常见的PPT生成过程任何功能均不依赖系统或第三方组件  
Linux或者Windows系统下都可以正常使用  
针对Windows生成视频和图片功能是通过vbs调用Windows系统Office实现的，仅这两个功能依赖于Windows系统
  
#### 授权协议
Apache-2.0协议授权  
即允许用于：商业用途、修改、分配、专利用途、私人使用  
但不允许注册为商标，也不为程序负责、不做可用性保证